#include <mono/metadata/mono-config.h>
#include <mono/metadata/assembly.h>

extern const unsigned char assembly_data_simple_exe [];
static const MonoBundledAssembly assembly_bundle_simple_exe = {"simple.exe", assembly_data_simple_exe, 3072};
extern const unsigned char assembly_data_System_Windows_Forms_dll [];
static const MonoBundledAssembly assembly_bundle_System_Windows_Forms_dll = {"System.Windows.Forms.dll", assembly_data_System_Windows_Forms_dll, 3038720};
extern const unsigned char assembly_config_System_Windows_Forms_dll [];
extern const unsigned char assembly_data_mscorlib_dll [];
static const MonoBundledAssembly assembly_bundle_mscorlib_dll = {"mscorlib.dll", assembly_data_mscorlib_dll, 2990080};
extern const unsigned char assembly_data_System_dll [];
static const MonoBundledAssembly assembly_bundle_System_dll = {"System.dll", assembly_data_System_dll, 1754112};
extern const unsigned char assembly_data_Mono_Security_dll [];
static const MonoBundledAssembly assembly_bundle_Mono_Security_dll = {"Mono.Security.dll", assembly_data_Mono_Security_dll, 294912};
extern const unsigned char assembly_data_System_Configuration_dll [];
static const MonoBundledAssembly assembly_bundle_System_Configuration_dll = {"System.Configuration.dll", assembly_data_System_Configuration_dll, 128000};
extern const unsigned char assembly_data_System_Xml_dll [];
static const MonoBundledAssembly assembly_bundle_System_Xml_dll = {"System.Xml.dll", assembly_data_System_Xml_dll, 1307648};
extern const unsigned char assembly_data_System_Security_dll [];
static const MonoBundledAssembly assembly_bundle_System_Security_dll = {"System.Security.dll", assembly_data_System_Security_dll, 132608};
extern const unsigned char assembly_data_System_Drawing_dll [];
static const MonoBundledAssembly assembly_bundle_System_Drawing_dll = {"System.Drawing.dll", assembly_data_System_Drawing_dll, 450560};
extern const unsigned char assembly_config_System_Drawing_dll [];
extern const unsigned char assembly_data_Mono_WebBrowser_dll [];
static const MonoBundledAssembly assembly_bundle_Mono_WebBrowser_dll = {"Mono.WebBrowser.dll", assembly_data_Mono_WebBrowser_dll, 177152};
extern const unsigned char assembly_config_Mono_WebBrowser_dll [];
extern const unsigned char assembly_data_Mono_Posix_dll [];
static const MonoBundledAssembly assembly_bundle_Mono_Posix_dll = {"Mono.Posix.dll", assembly_data_Mono_Posix_dll, 194560};
extern const unsigned char assembly_data_System_Runtime_Serialization_Formatters_Soap_dll [];
static const MonoBundledAssembly assembly_bundle_System_Runtime_Serialization_Formatters_Soap_dll = {"System.Runtime.Serialization.Formatters.Soap.dll", assembly_data_System_Runtime_Serialization_Formatters_Soap_dll, 39936};
extern const unsigned char assembly_data_Accessibility_dll [];
static const MonoBundledAssembly assembly_bundle_Accessibility_dll = {"Accessibility.dll", assembly_data_Accessibility_dll, 12288};
extern const unsigned char assembly_data_System_Data_dll [];
static const MonoBundledAssembly assembly_bundle_System_Data_dll = {"System.Data.dll", assembly_data_System_Data_dll, 856064};
extern const unsigned char assembly_config_System_Data_dll [];
extern const unsigned char assembly_data_Mono_Data_Tds_dll [];
static const MonoBundledAssembly assembly_bundle_Mono_Data_Tds_dll = {"Mono.Data.Tds.dll", assembly_data_Mono_Data_Tds_dll, 99328};
extern const unsigned char assembly_data_System_Transactions_dll [];
static const MonoBundledAssembly assembly_bundle_System_Transactions_dll = {"System.Transactions.dll", assembly_data_System_Transactions_dll, 32256};
extern const unsigned char assembly_data_System_EnterpriseServices_dll [];
static const MonoBundledAssembly assembly_bundle_System_EnterpriseServices_dll = {"System.EnterpriseServices.dll", assembly_data_System_EnterpriseServices_dll, 47616};

static const MonoBundledAssembly *bundled [] = {
	&assembly_bundle_simple_exe,
	&assembly_bundle_System_Windows_Forms_dll,
	&assembly_bundle_mscorlib_dll,
	&assembly_bundle_System_dll,
	&assembly_bundle_Mono_Security_dll,
	&assembly_bundle_System_Configuration_dll,
	&assembly_bundle_System_Xml_dll,
	&assembly_bundle_System_Security_dll,
	&assembly_bundle_System_Drawing_dll,
	&assembly_bundle_Mono_WebBrowser_dll,
	&assembly_bundle_Mono_Posix_dll,
	&assembly_bundle_System_Runtime_Serialization_Formatters_Soap_dll,
	&assembly_bundle_Accessibility_dll,
	&assembly_bundle_System_Data_dll,
	&assembly_bundle_Mono_Data_Tds_dll,
	&assembly_bundle_System_Transactions_dll,
	&assembly_bundle_System_EnterpriseServices_dll,
	NULL
};

static char *image_name = "simple.exe";

static void install_dll_config_files (void) {

	mono_register_config_for_assembly ("System.Windows.Forms.dll", assembly_config_System_Windows_Forms_dll);

	mono_register_config_for_assembly ("System.Drawing.dll", assembly_config_System_Drawing_dll);

	mono_register_config_for_assembly ("Mono.WebBrowser.dll", assembly_config_Mono_WebBrowser_dll);

	mono_register_config_for_assembly ("System.Data.dll", assembly_config_System_Data_dll);

}

static const char *config_dir = NULL;
void mono_mkbundle_init ()
{
	install_dll_config_files ();
	mono_register_bundled_assemblies(bundled);
}
int mono_main (int argc, char* argv[]);

#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#endif

static char **mono_options = NULL;

static int count_mono_options_args (void)
{
	const char *e = getenv ("MONO_BUNDLED_OPTIONS");
	const char *p, *q;
	int i, n;

	if (e == NULL)
		return 0;

	/* Don't bother with any quoting here. It is unlikely one would
	 * want to pass options containing spaces anyway.
	 */

	p = e;
	n = 1;
	while ((q = strchr (p, ' ')) != NULL) {
		n++;
		p = q + 1;
	}

	mono_options = malloc (sizeof (char *) * (n + 1));

	p = e;
	i = 0;
	while ((q = strchr (p, ' ')) != NULL) {
		mono_options[i] = malloc ((q - p) + 1);
		memcpy (mono_options[i], p, q - p);
		mono_options[i][q - p] = '\0';
		i++;
		p = q + 1;
	}
	mono_options[i++] = strdup (p);
	mono_options[i] = NULL;

	return n;
}


int main (int argc, char* argv[])
{
	char **newargs;
	int i, k = 0;

	newargs = (char **) malloc (sizeof (char *) * (argc + 2 + count_mono_options_args ()));

	newargs [k++] = argv [0];

	if (mono_options != NULL) {
		i = 0;
		while (mono_options[i] != NULL)
			newargs[k++] = mono_options[i++];
	}

	newargs [k++] = image_name;

	for (i = 1; i < argc; i++) {
		newargs [k++] = argv [i];
	}
	newargs [k] = NULL;
	
	if (config_dir != NULL && getenv ("MONO_CFG_DIR") == NULL)
		mono_set_dirs (getenv ("MONO_PATH"), config_dir);
	
	mono_mkbundle_init();

	return mono_main (k, newargs);
}
